#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Gainoptimizer
# GNU Radio version: 3.7.13.5
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from PyQt4.QtCore import QObject, pyqtSlot
from gnuradio import analog
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import qtgui
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from gnuradio.qtgui import Range, RangeWidget
from optparse import OptionParser
import sip
import sys
from gnuradio import qtgui


class gainOptimizer(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Gainoptimizer")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Gainoptimizer")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "gainOptimizer")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 30
        self.c_freq = c_freq = 436.5e6
        self.a_rf_gain = a_rf_gain = 30
        self.a_if_gain = a_if_gain = 30
        self.a_bb_gain = a_bb_gain = 30

        ##################################################
        # Blocks
        ##################################################
        self._samp_rate_range = Range(10, 500, 10, 30, 200)
        self._samp_rate_win = RangeWidget(self._samp_rate_range, self.set_samp_rate, 'Sample Rate (kS/s)', "counter_slider", float)
        self.top_grid_layout.addWidget(self._samp_rate_win)
        self.qtgui_time_sink_x_0 = qtgui.time_sink_c(
        	1024, #size
        	samp_rate*1000, #samp_rate
        	"AcubeSAT Ground Station | SDR Gain Optimization Tool", #name
        	1 #number of inputs
        )
        self.qtgui_time_sink_x_0.set_update_time(0.10)
        self.qtgui_time_sink_x_0.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0.enable_tags(-1, True)
        self.qtgui_time_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_x_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0.enable_grid(True)
        self.qtgui_time_sink_x_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0.enable_control_panel(False)
        self.qtgui_time_sink_x_0.enable_stem_plot(False)

        if not True:
          self.qtgui_time_sink_x_0.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(2):
            if len(labels[i]) == 0:
                if(i % 2 == 0):
                    self.qtgui_time_sink_x_0.set_line_label(i, "Re{{Data {0}}}".format(i/2))
                else:
                    self.qtgui_time_sink_x_0.set_line_label(i, "Im{{Data {0}}}".format(i/2))
            else:
                self.qtgui_time_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_time_sink_x_0_win)
        self._c_freq_options = (436.5e6, 2425e6, )
        self._c_freq_labels = ('UHF (436.5 MHz)', 'S band (2425 MHz)', )
        self._c_freq_group_box = Qt.QGroupBox('Band Selection')
        self._c_freq_box = Qt.QHBoxLayout()
        class variable_chooser_button_group(Qt.QButtonGroup):
            def __init__(self, parent=None):
                Qt.QButtonGroup.__init__(self, parent)
            @pyqtSlot(int)
            def updateButtonChecked(self, button_id):
                self.button(button_id).setChecked(True)
        self._c_freq_button_group = variable_chooser_button_group()
        self._c_freq_group_box.setLayout(self._c_freq_box)
        for i, label in enumerate(self._c_freq_labels):
        	radio_button = Qt.QRadioButton(label)
        	self._c_freq_box.addWidget(radio_button)
        	self._c_freq_button_group.addButton(radio_button, i)
        self._c_freq_callback = lambda i: Qt.QMetaObject.invokeMethod(self._c_freq_button_group, "updateButtonChecked", Qt.Q_ARG("int", self._c_freq_options.index(i)))
        self._c_freq_callback(self.c_freq)
        self._c_freq_button_group.buttonClicked[int].connect(
        	lambda i: self.set_c_freq(self._c_freq_options[i]))
        self.top_grid_layout.addWidget(self._c_freq_group_box)
        self.analog_noise_source_x_0 = analog.noise_source_c(analog.GR_GAUSSIAN, 0.17, 0)
        self._a_rf_gain_range = Range(0, 70, 1, 30, 200)
        self._a_rf_gain_win = RangeWidget(self._a_rf_gain_range, self.set_a_rf_gain, 'RF Gain', "counter_slider", float)
        self.top_grid_layout.addWidget(self._a_rf_gain_win)
        self._a_if_gain_range = Range(0, 70, 1, 30, 200)
        self._a_if_gain_win = RangeWidget(self._a_if_gain_range, self.set_a_if_gain, 'IF Gain', "counter_slider", float)
        self.top_grid_layout.addWidget(self._a_if_gain_win)
        self._a_bb_gain_range = Range(0, 70, 1, 30, 200)
        self._a_bb_gain_win = RangeWidget(self._a_bb_gain_range, self.set_a_bb_gain, 'BB Gain', "counter_slider", float)
        self.top_grid_layout.addWidget(self._a_bb_gain_win)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_noise_source_x_0, 0), (self.qtgui_time_sink_x_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "gainOptimizer")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.qtgui_time_sink_x_0.set_samp_rate(self.samp_rate*1000)

    def get_c_freq(self):
        return self.c_freq

    def set_c_freq(self, c_freq):
        self.c_freq = c_freq
        self._c_freq_callback(self.c_freq)

    def get_a_rf_gain(self):
        return self.a_rf_gain

    def set_a_rf_gain(self, a_rf_gain):
        self.a_rf_gain = a_rf_gain

    def get_a_if_gain(self):
        return self.a_if_gain

    def set_a_if_gain(self, a_if_gain):
        self.a_if_gain = a_if_gain

    def get_a_bb_gain(self):
        return self.a_bb_gain

    def set_a_bb_gain(self, a_bb_gain):
        self.a_bb_gain = a_bb_gain


def main(top_block_cls=gainOptimizer, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
